# Push button
| Ref        | Maker    | Price | Main char         |
|------------|----------|-------|-------------------|
| SKRHADE010 | ALPS     | 1€    | 5 way push button |
| JS1300AQ   | E-Switch | 1.6€  | 5 way push button |

# Current limiting
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| MIC2444A/MIC2548A | Microchip | 1€ | 0.1-A1.5A | Rdon = 120mOhms, 2.7-5.5V | MSOP-8 / SOIC-8
| FPF210x           | Fairchild | 1€ | 400mA | Rdon = 160mOhms, 1.8-5.5V | SOT-23-5 |
| TPS2500DRCR       | Texas     | 2.6€ | 0.13-1.4A | Rdon=50mOhms, 1.8-5.25V, Boost converter | VSON-10
| MAX4995xx         | Maxim     | 3.8€ | 50-600mA, autoretry | Rdon=130mOhms, 1.7-5.5V | SOT23 / TDFN

# Rearmable fuse
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| 0ZCJ0025AF2E | Bel fuse | 0.1€ | hold=0.25A, trip=0.5A | Rdon = 2.3, 24V | 1206
| 0ZCJ0075AF2E | Bel fuse | 0.15€ | hold=0.75A, trip=1.5A | Rdon = 0.29, 16V | 1206

# Digital potentiometer
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| MCP401x   | Microchip     | 0.5€ | 2.1k,5.0k,10.0k,50.0k 20% | 64 steps, 1.8-5.5V
| AD5112xxx | Analog Device | 1.8# | 5k,10k,80k             8% | 64 steps, 1.8-5.5V

# DC/DC
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| TSR 0.5-2433        | Traco  | 6€   | 3.3V | Step down, 500mA, 4.75-32V, ripple 30mV, C not needed | Through Hole
| ROF-78E3.3-0.5SMD-R | Recom  | 3.1€ | 3.3V | Step down, 500mA, 5-36V, ripple 100mV, C not needed | PCB
| LXDC2HN33A-136      | Murata | 1€   | 3.3V | Step down, 300mA, 2.3-5.5V, ripple 15mV, need C 4.7µ / 10µ| 
| LXDC2UR33A-122      | Murata | 1.4€ | 3.3V | Step down, 600mA, 3.6-5.5V, ripple 15mV, C not needed | 2.5x2.3
| P7805-Q24-S3-S      | CUI    | 5€   | 3.3V | Step down, 500mA, 4.75-28V, ripple 30mV, need C 10µ / 10µ | Through hole
| D24V5F3             | Pololu | 5€   | 3.3V | Step Down, 500mA | PCB
| S7V8F3              | Pololu | 6€   | 3.3V | Step Up/Down, 500mA |  PCB

# MOSFET
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| BSS806N H6327 | Infineon | 0.3€ | MOSFET N-Ch 20V 2.3A | |  SOT-23-3
| BSD840N H6327 | Infineon | 0.4€ | MOSFET N-Ch 20V 880mA | |  SOT-363-6
| DMP3056L-7 | Diodes Inc. | 0.4€ | MOSFET P-Ch 30V 4.3A | | SOT-23-3
| DMP2035U-7 | Diodes Inc | 0.4€ | MOSFET P-Ch 20V 3.6A | | SOT-23-3
| IRF9389TRPBF | Infineon | 0.4€ | MOSFET 30V Dual N and P Ch HEXFET 20-8 20VGS 4A | | SO-8
| BSL215C H6327 | Infineon | 0.4€ | MOSFET SMALL SIGNAL+P-CH 1.5A || TSOP-6 (SOT23-6)
| FDS4435BZ | Fairchild | 0.7€ | MOSFET 30V.PCH POWER TRENCH MOSFET 8.8A | RdsOn=16m| SO-8

# Isolation
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
ADUM1201ARZ | Analog | 2.5€ | 2 Ch, BiDi, 1Mb/s, 45ns | 600µA, 2.7-5.5V | SOIC-8

# Connector
| Ref | Maker | Price | Main char | Other  | Footprint |
|-----|-------|-------|------|-------|-------------|
| B2B-PH-SM4-TB | JST | 0.5€ | 2A / 100V / 2 pin
